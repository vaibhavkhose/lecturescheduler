# University Lecture Scheduler

## This solution contains:
### Scheduler.Api
    + APIs to add and read student.
	+ APIs to enroll a student to lecture
	+ APIs to add and read lecture theatres
	+ APIs to ad and read subjects
	+ APIs to add and read lecture schedules
	  
### Scheduler.Domain
    + Domain model for lecture scheduler subdomain
	+ Abstract repositories

### Scheduler.Common
    + Common base classes for domain entities e.g. IEntity, ValueObject

### Scheduler.Dtos
    + Request and response objects
	+ These objects are mapped to domain entities in request handlers

### Scheduler.Infrastructure
    + Repository implementation for entities
	+ EF configurations
	+ Request handlers using mediatR
	
### Unit Tests
    + Unit tests using xunit, NSubstitute and fluent assersions
	+ Unit tests are not fully complete yet

### Framework and libraries used
    + .NET core 3.1
	+ EF Core
	+ MediatR
    + FluentValidation for request validation
    + Swashbuckle for swagger integration

## How to Run the application
	+ This application is developed using visual studio code. Can be opened into VS 2019 v16 and above.
	+ Run from command promp
	   - dotnet run (from Api directory)
	   - run tests - dotnet test (from unittest folder)

