﻿using System;

namespace scheduler.common
{
    public interface IEntity
    {
        int Id { get; }
    }
}
