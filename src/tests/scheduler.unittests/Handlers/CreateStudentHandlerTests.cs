﻿using NSubstitute;
using Xunit;
using scheduler.infrastructure.Handlers;
using scheduler.dtos.Student;
using scheduler.domain.Repositories;
using MediatR;
using System.Threading.Tasks;
using scheduler.domain.models;
using System.Threading;
using FluentAssertions;

namespace scheduler.unittests.Handlers
{
    public class CreateStudentHandlerTests
    {
        private readonly IStudentRepository _studentRepository;
        private readonly IRequestHandler<CreateStudentRequest, CreateStudentResponse> _sut;

        public CreateStudentHandlerTests()
        {
            _studentRepository = Substitute.For<IStudentRepository>();  
            _sut = new CreateStudentHandler(_studentRepository);
        }

        [Fact]
        public async Task hanle_should_handle_create_student_request_successfully()
        {
            _studentRepository.Add(Arg.Any<Student>()).Returns(Task.FromResult(new Student(new FullName("firstname", "lastname"), new ContactDetails("1234", "a@a.gmail")){
                Id = 1
            }));
           
            var createStudentRequest = new CreateStudentRequest{ 
                FirstName = "firstname",
                LastName = "lastname",
                EmailId = "a@g.com"
            };

            var response = await _sut.Handle(createStudentRequest, new CancellationToken());
            response.Should().NotBeNull();
            await _studentRepository.ReceivedWithAnyArgs(1).Add(Arg.Any<Student>());
        }

        //TODO - Addd more test cases round validations scenarios.
    }
}
