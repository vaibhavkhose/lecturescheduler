﻿using System;
using NSubstitute;
using Xunit;
using scheduler.infrastructure.Handlers;
using scheduler.dtos.Lecture;
using scheduler.domain.Repositories;
using MediatR;
using System.Threading.Tasks;
using scheduler.domain.models;
using System.Threading;
using FluentAssertions;

namespace scheduler.unittests.Handlers
{
    public class CreateLectureHandlerTests
    {
        private readonly ILectureRepository _lectureRepository;
        private readonly ILectureTheatreRepository _lectureTheatreRepository;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IRequestHandler<CreateLectureRequest, CreateLectureResponse> _sut;

        public CreateLectureHandlerTests()
        {
            _lectureRepository = Substitute.For<ILectureRepository>();
            _lectureTheatreRepository = Substitute.For<ILectureTheatreRepository>();
            _subjectRepository = Substitute.For<ISubjectRepository>();
            
            _sut = new CreateLectureHandler(_lectureRepository, _lectureTheatreRepository, _subjectRepository);
        }

        [Fact]
        public async Task hanle_should_handle_create_lecture_request_successfully()
        {
            _lectureTheatreRepository.FindById(1).Returns(Task.FromResult(new LectureTheatre{
                Id = 1,
                Name = "my lecture theatre"
            }));
            _subjectRepository.FindById(Arg.Any<int>()).Returns(Task.FromResult(new Subject{
                Id = 1,
                Title = "my subject"
            }));
            _lectureRepository.Add(Arg.Any<Lecture>()).Returns(new Lecture());

            var createLectureRequest = new CreateLectureRequest{ 
                SubjectId = 1,
                LectureTheatreId = 1,
                DurationInMins = 30,
                ScheduleDate = DateTime.Now.AddDays(3)
            };

            var response = await _sut.Handle(createLectureRequest, new CancellationToken());
            response.Should().NotBeNull();
            await _lectureRepository.ReceivedWithAnyArgs(1).Add(Arg.Any<Lecture>());
            await _lectureTheatreRepository.Received(1).FindById(1);
            await _subjectRepository.Received(1).FindById(1);
        }

        //TODO - Addd more test cases round validations scenarios.
    }
}
