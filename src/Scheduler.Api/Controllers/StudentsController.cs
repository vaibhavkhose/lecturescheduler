using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using scheduler.domain.Events;
using scheduler.dtos.Student;
using System.Threading.Tasks;

namespace scheduler.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<StudentsController> _logger;
        public StudentsController(ILogger<StudentsController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<GetStudentResponse> Get()
        {
            return await _mediator.Send(new GetStudentRequest());
        }

        [HttpPost]
        public async Task<CreateStudentResponse> Create(CreateStudentRequest createStudentRequest)
        {
            return await _mediator.Send(createStudentRequest);
        }

        [HttpPost]
        [Route("{id}/Enroll")]
        public async Task<EnrollToLectureResponse> Create(int id, EnrollToLectureRequest enrollToLectureRequest)
        {
            enrollToLectureRequest.StudentId = id;
            var result = await _mediator.Send(enrollToLectureRequest);

            await _mediator.Publish(new EnrollmentComplete());
            return result;
        }
    }
}