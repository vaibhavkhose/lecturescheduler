using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using scheduler.dtos.Lecture;
using System.Threading.Tasks;

namespace scheduler.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LectureController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<LectureController> _logger;
        public LectureController(ILogger<LectureController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<GetLectureResponse> Get()
        {
            return await _mediator.Send(new GetLectureRequest());
        }

        [HttpPost]
        public async Task<CreateLectureResponse> Create(CreateLectureRequest createLectureRequest)
        {
            return await _mediator.Send(createLectureRequest);
        }
    }
}