using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using scheduler.dtos.Subject;
using System.Threading.Tasks;

namespace scheduler.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SubjectController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<SubjectController> _logger;
        public SubjectController(ILogger<SubjectController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<GetSubjectResponse> Get()
        {
            return await _mediator.Send(new GetSubjectRequest());
        }

        [HttpPost]
        public async Task<CreateSubjectResponse> Create(CreateSubjectRequest createSubjectRequest)
        {
            return await _mediator.Send(createSubjectRequest);
        }
    }
}