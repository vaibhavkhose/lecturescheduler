using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using scheduler.dtos.LectureTheatre;
using System.Threading.Tasks;

namespace scheduler.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LectureTheatreController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<LectureTheatreController> _logger;
        public LectureTheatreController(ILogger<LectureTheatreController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<GetLectureTheatreResponse> Get()
        {
            return await _mediator.Send(new GetLectureTheatreRequest());
        }

        [HttpPost]
        public async Task<CreateLectureTheatreResponse> Create(CreateLectureTheatreRequest createLectureTheatreRequest)
        {
            return await _mediator.Send(createLectureTheatreRequest);
        }
    }
}