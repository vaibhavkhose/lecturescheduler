using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using scheduler.infrastructure;
using scheduler.infrastructure.Handlers;
using Microsoft.EntityFrameworkCore.Diagnostics;
using scheduler.domain.Repositories;
using scheduler.infrastructure.Repositories;
using FluentValidation.AspNetCore;
using scheduler.api.RequestValidators;

namespace scheduler.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMediatR(typeof(CreateStudentHandler));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
            services.AddMvc(opt =>
            {
                opt.Filters.Add(typeof(DbContextTransactionFilter));
            }).AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CreateSubjectRequestValidator>());
            services.AddDbContext<SchedulerDbContext>(options =>
            {
                options.UseInMemoryDatabase("LetureScheduler");
                options.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            });

            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<ILectureTheatreRepository, LectureTheatreRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
               {
                   c.SwaggerEndpoint("/swagger/v1/swagger.json", "Lecture Scheduler");
                   c.RoutePrefix = string.Empty;
               });
        }
    }
}
