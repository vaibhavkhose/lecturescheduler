﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using scheduler.dtos.Subject;

namespace scheduler.api.RequestValidators
{
    public class CreateSubjectRequestValidator : AbstractValidator<CreateSubjectRequest>
    {
        public CreateSubjectRequestValidator()
        {
            RuleFor(x => x.Title).NotNull();
        }

        //TODO write request validators for other requests
    }
}
