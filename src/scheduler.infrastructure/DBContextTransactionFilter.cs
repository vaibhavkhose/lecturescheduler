﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace scheduler.infrastructure
{
    public class DbContextTransactionFilter : IAsyncActionFilter
    {
        private readonly SchedulerDbContext _dbContext;

        public DbContextTransactionFilter(SchedulerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            try
            {
                await _dbContext.BeginTransactionAsync();

                var actionExecuted = await next();
                if (actionExecuted.Exception != null && !actionExecuted.ExceptionHandled)
                {
                    _dbContext.RollbackTransaction();

                }
                else
                {
                    await _dbContext.CommitTransactionAsync();

                }
            }
            catch (Exception)
            {
                _dbContext.RollbackTransaction();
                throw;
            }
        }
    }
}
