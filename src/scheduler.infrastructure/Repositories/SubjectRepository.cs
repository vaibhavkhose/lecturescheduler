﻿using Microsoft.EntityFrameworkCore;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.infrastructure.Repositories
{
    public class SubjectRepository : ISubjectRepository
    {
        private readonly SchedulerDbContext _schedulerDbContext;

        public SubjectRepository(SchedulerDbContext schedulerDbContext)
        {
            _schedulerDbContext = schedulerDbContext;
        }
        public async Task<Subject> Add(Subject subject)
        {
            await _schedulerDbContext.Subjects.AddAsync(subject);
            return subject;
        }

        public async Task<List<Subject>> FindAll()
        {
            return await _schedulerDbContext.Subjects.ToListAsync();
        }

        public async Task<Subject> FindById(int id)
        {
            return await _schedulerDbContext.Subjects.FirstOrDefaultAsync(s => s.Id.Equals(id));
        }

        public void Update(Subject subject)
        {
            _schedulerDbContext.Subjects.Update(subject);
        }
    }
}
