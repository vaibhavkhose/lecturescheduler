﻿using Microsoft.EntityFrameworkCore;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.infrastructure.Repositories
{
    public class LectureTheatreRepository : ILectureTheatreRepository
    {
        private readonly SchedulerDbContext _schedulerDbContext;

        public LectureTheatreRepository(SchedulerDbContext schedulerDbContext)
        {
            _schedulerDbContext = schedulerDbContext;
        }
        public async Task<LectureTheatre> Add(LectureTheatre lectureTheatre)
        {
            await _schedulerDbContext.LectureTheatres.AddAsync(lectureTheatre);
            return lectureTheatre;
        }

        public async Task<List<LectureTheatre>> FindAll()
        {
            return await _schedulerDbContext.LectureTheatres.ToListAsync();
        }

        public async Task<LectureTheatre> FindById(int id)
        {
            return await _schedulerDbContext.LectureTheatres.FirstOrDefaultAsync(s => s.Id.Equals(id));
        }

        public Task Update(LectureTheatre lectureTheatre)
        {
            throw new NotImplementedException();
        }
    }
}
