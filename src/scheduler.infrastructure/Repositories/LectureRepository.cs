﻿using Microsoft.EntityFrameworkCore;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.infrastructure.Repositories
{
    public class LectureRepository : ILectureRepository
    {
        private readonly SchedulerDbContext _schedulerDbContext;

        public LectureRepository(SchedulerDbContext schedulerDbContext)
        {
            _schedulerDbContext = schedulerDbContext;
        }
        public async Task<Lecture> Add(Lecture lecture)
        {
            await _schedulerDbContext.Lectures.AddAsync(lecture);
            return lecture;
        }

        public async Task<List<Lecture>> FindAll()
        {
            return await _schedulerDbContext.Lectures.ToListAsync();
        }

        public async Task<Lecture> FindById(int id)
        {
            return await _schedulerDbContext.Lectures.FirstOrDefaultAsync(s => s.Id.Equals(id));
        }

        public Task Update(Lecture student)
        {
            throw new NotImplementedException();
        }
    }
}
