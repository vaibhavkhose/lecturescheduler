﻿using Microsoft.EntityFrameworkCore;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.infrastructure.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        private readonly SchedulerDbContext _schedulerDbContext;

        public StudentRepository(SchedulerDbContext schedulerDbContext)
        {
            _schedulerDbContext = schedulerDbContext;
        }
        public async Task<Student> Add(Student student)
        {
            await _schedulerDbContext.Students.AddAsync(student);
            return student;
        }

        public async Task<List<Student>> FindAll()
        {
            return await _schedulerDbContext.Students.ToListAsync();
        }

        public async Task<Student> FindById(int id)
        {
            return await _schedulerDbContext.Students.FirstOrDefaultAsync(s => s.Id.Equals(id));
        }

        public Task Update(Student student)
        {
            throw new NotImplementedException();
        }
    }
}
