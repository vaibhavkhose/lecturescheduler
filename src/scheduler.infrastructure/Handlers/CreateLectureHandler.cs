﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using scheduler.dtos.Lecture;

namespace scheduler.infrastructure.Handlers
{
    public class CreateLectureHandler : IRequestHandler<CreateLectureRequest, CreateLectureResponse>
    {
        private readonly ILectureRepository _lectureRepository;
        private readonly ILectureTheatreRepository _lectureTheatreRepository;
        private readonly ISubjectRepository _subjectRepository;

        public CreateLectureHandler(ILectureRepository lectureRepository, ILectureTheatreRepository lectureTheatreRepository, ISubjectRepository subjectRepository)
        {
            _lectureRepository = lectureRepository;
            _lectureTheatreRepository = lectureTheatreRepository;
            _subjectRepository = subjectRepository;
        }

        public async Task<CreateLectureResponse> Handle(CreateLectureRequest request, CancellationToken cancellationToken)
        {
            var lectureTheatre = await _lectureTheatreRepository.FindById(request.LectureTheatreId);
            var subject = await _subjectRepository.FindById(request.SubjectId);

            //validate if lecture theatre and subjects are valid
            //validate if lecture theatre is available at required time and if it is not duplicate booking

            var newLecture = await _lectureRepository.Add(new Lecture {  Theatre = lectureTheatre, Subject = subject, Period = new Schedule(request.ScheduleDate, request.DurationInMins) });
            return new CreateLectureResponse { LectureId = newLecture.Id };
        }
    }
}
