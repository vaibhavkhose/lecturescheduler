﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using scheduler.dtos.LectureTheatre;

namespace scheduler.infrastructure.Handlers
{
    public class CreateLectureTheatreHandler : IRequestHandler<CreateLectureTheatreRequest, CreateLectureTheatreResponse>
    {
        private readonly ILectureTheatreRepository _lectureTheatreRepository;
        public CreateLectureTheatreHandler(ILectureTheatreRepository lectureTheatreRepository)
        {
            _lectureTheatreRepository = lectureTheatreRepository;
        }

        public async Task<CreateLectureTheatreResponse> Handle(CreateLectureTheatreRequest request, CancellationToken cancellationToken)
        {
            var newLectureTheatre = await _lectureTheatreRepository.Add(new LectureTheatre { Name = "Lecture Hall 1" });
            return new CreateLectureTheatreResponse { LectureTheatreId = newLectureTheatre.Id };
        }
    }
}
