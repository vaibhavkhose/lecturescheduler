﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using scheduler.dtos.Student;

namespace scheduler.infrastructure.Handlers
{
    public class CreateStudentHandler : IRequestHandler<CreateStudentRequest, CreateStudentResponse>
    {
        private readonly IStudentRepository _studentRepository;
        public CreateStudentHandler(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<CreateStudentResponse> Handle(CreateStudentRequest request, CancellationToken cancellationToken)
        {
            var newStudent = await _studentRepository.Add(new Student(new FullName(request.FirstName, request.LastName), new ContactDetails(request.MobilePhone, request.EmailId)));
            return new CreateStudentResponse { StudentId = newStudent.Id };
        }
    }
}
