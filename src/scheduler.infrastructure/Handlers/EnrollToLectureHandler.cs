﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.Repositories;
using scheduler.dtos.Student;

namespace scheduler.infrastructure.Handlers
{
    public class EnrollToLectureHandler : IRequestHandler<EnrollToLectureRequest, EnrollToLectureResponse>
    {
        private readonly IStudentRepository _studentRepository;
        private readonly ILectureRepository _lectureRepository;
        public EnrollToLectureHandler(IStudentRepository studentRepository, ILectureRepository lectureRepository)
        {
            _studentRepository = studentRepository;
            _lectureRepository = lectureRepository;
        }

        public async Task<EnrollToLectureResponse> Handle(EnrollToLectureRequest request, CancellationToken cancellationToken)
        {
            var student = await _studentRepository.FindById(request.StudentId);
            var lecture = await _lectureRepository.FindById(request.LectureId);

            //validate student and lecture exists
            //validate if student is not already enrolled for lecture

            if (lecture?.Theatre.Capacity < lecture.Enrollments.Count)
            {
                lecture.Theatre.Capacity += 1;
                student.EnrollForLecture(lecture);
                await _studentRepository.Update(student);
            }
            else
            {
                //throw new NoSpotAvailableException();
            }
            return new EnrollToLectureResponse();
        }
    }
}
