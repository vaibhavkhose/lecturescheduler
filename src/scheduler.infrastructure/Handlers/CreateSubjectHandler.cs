﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.models;
using scheduler.domain.Repositories;
using scheduler.dtos.Subject;

namespace scheduler.infrastructure.Handlers
{
    public class CreateSubjectHandler : IRequestHandler<CreateSubjectRequest, CreateSubjectResponse>
    {
        private readonly ISubjectRepository _subjectRepository;
        public CreateSubjectHandler(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public async Task<CreateSubjectResponse> Handle(CreateSubjectRequest request, CancellationToken cancellationToken)
        {
            var newSubject = await _subjectRepository.Add(new Subject { Title = request.Title });
            return new CreateSubjectResponse { SubjectId = newSubject.Id };
        }
    }
}
