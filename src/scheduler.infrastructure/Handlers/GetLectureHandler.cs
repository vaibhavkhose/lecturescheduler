﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.Repositories;
using scheduler.dtos.Lecture;

namespace scheduler.infrastructure.Handlers
{
    public class GetLectureHandler : IRequestHandler<GetLectureRequest, GetLectureResponse>
    {
        private readonly ILectureRepository _lectureRepository;
        public GetLectureHandler(ILectureRepository lectureRepository)
        {
            _lectureRepository = lectureRepository;
        }

        public async Task<GetLectureResponse> Handle(GetLectureRequest request, CancellationToken cancellationToken)
        {
            var lectures = await _lectureRepository.FindAll();
            return new GetLectureResponse
            {
                Lectures = lectures.Select(s => new LectureResponse
                {
                    SubjectName = s.Subject?.Title,
                    DurationInMinutes = s.Period.DurationInMins,
                    Schedule = s.Period.Start,
                    LectureTheatreName = s.Theatre.Name,
                    Enrollments = s.Enrollments.Select(e => new LectureResponse.EnrollmentResponse
                    {
                        EmailId = e.ContactDetails.EmailId,
                        Name = e.FullName.AsFormattedName()
                    }).ToList()
                }).ToList()
            };
        }
    }
}
