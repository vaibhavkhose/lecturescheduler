﻿using MediatR;
using scheduler.domain.Events;
using System.Threading;
using System.Threading.Tasks;

namespace scheduler.infrastructure.Handlers
{
    public class EnrollmentCompleteNotificationHandler : INotificationHandler<EnrollmentComplete>
    {       
        public Task Handle(EnrollmentComplete notification, CancellationToken cancellationToken)
        {
            //send email or do other action
            return Task.CompletedTask;
        }
    }
}
