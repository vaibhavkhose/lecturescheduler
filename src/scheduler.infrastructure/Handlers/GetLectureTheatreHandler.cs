﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.Repositories;
using scheduler.dtos.Lecture;
using scheduler.dtos.LectureTheatre;

namespace scheduler.infrastructure.Handlers
{
    public class GetLectureTheatreHandler : IRequestHandler<GetLectureTheatreRequest, GetLectureTheatreResponse>
    {
        private readonly ILectureTheatreRepository _lectureTheatreRepository;
        public GetLectureTheatreHandler(ILectureTheatreRepository lectureTheatreRepository)
        {
            _lectureTheatreRepository = lectureTheatreRepository;
        }

        public async Task<GetLectureTheatreResponse> Handle(GetLectureTheatreRequest request, CancellationToken cancellationToken)
        {
            var lectureTheatres = await _lectureTheatreRepository.FindAll();
            return new GetLectureTheatreResponse
            {
                LectureTheatres = lectureTheatres.Select(l => new LectureTheatreResponse
                {
                    Lectures = l.Lectures != null && l.Lectures.Any() ? l.Lectures.Select(a => new LectureResponse { SubjectName = a.Subject.Title, Schedule = a.Period.Start, DurationInMinutes = a.Period.DurationInMins }).ToList() : null,
                    Name = l.Name
                }).ToList()
            };
        }
    }
}
