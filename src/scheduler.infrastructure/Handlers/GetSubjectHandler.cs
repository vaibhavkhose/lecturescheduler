﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.Repositories;
using scheduler.dtos.Subject;

namespace scheduler.infrastructure.Handlers
{
    public class GetSubjectHandler : IRequestHandler<GetSubjectRequest, GetSubjectResponse>
    {
        private readonly ISubjectRepository _subjectRepository;
        public GetSubjectHandler(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public async Task<GetSubjectResponse> Handle(GetSubjectRequest request, CancellationToken cancellationToken)
        {
            var subjects = await _subjectRepository.FindAll();
            return new GetSubjectResponse
            {
                Subjects = subjects.Select(s => new SubjectResponse
                {
                    Title = s.Title,
                    Lectures = s.Lectures?.Select(l => new SubjectResponse.LectureResponse
                    {
                        Schedule = l.Period.Start,
                        DurationInMinutes = l.Period.DurationInMins
                    }).ToList()
                }).ToList()
            };
        }
    }
}
