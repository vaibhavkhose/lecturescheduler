﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using scheduler.domain.Repositories;
using scheduler.dtos.Student;

namespace scheduler.infrastructure.Handlers
{
    public class GetStudentHandler : IRequestHandler<GetStudentRequest, GetStudentResponse>
    {
        private readonly IStudentRepository _studentRepository;
        public GetStudentHandler(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<GetStudentResponse> Handle(GetStudentRequest request, CancellationToken cancellationToken)
        {
            var students = await _studentRepository.FindAll();
            return new GetStudentResponse
            {
                Students = students.Select(s => new StudentResponse
                {
                    FullName = s.FullName.AsFormattedName(),
                    Lectures = s.Enrollments.Select(e => new StudentResponse.LectureResponse
                    {
                        LectureTheatreName = e.Theatre.Name,
                        SubjectName = e.Subject.Title,
                        ScheduleDate = e.Period.Start,
                        DurationInMins = e.Period.DurationInMins
                    }).ToList()
                }).ToList()
            };
        }
    }
}
