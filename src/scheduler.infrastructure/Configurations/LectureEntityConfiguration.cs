﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using scheduler.domain.models;

namespace scheduler.infrastructure.Configurations
{
    public class LectureEntityConfiguration : IEntityTypeConfiguration<Lecture>
    {
        public void Configure(EntityTypeBuilder<Lecture> builder)
        {
            builder.ToTable("Lecture");
            builder.HasKey(e => e.Id);
            builder.OwnsOne(p => p.Period)
                                        .Property(p => p.Start).HasColumnName("Start");
            builder.OwnsOne(p => p.Period)
                                        .Property(p => p.DurationInMins).HasColumnName("Duration");        
        }
    }
}
