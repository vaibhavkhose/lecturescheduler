﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using scheduler.domain.models;

namespace scheduler.infrastructure.Configurations
{
    public class StudentEntityConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable("Student");
            builder.HasKey(e => e.Id);
            builder.OwnsOne(p => p.FullName)
                                        .Property(p => p.FirstName).HasColumnName("FirstName");
            builder.OwnsOne(p => p.FullName)
                                        .Property(p => p.LastName).HasColumnName("LastName");

            builder.OwnsOne(p => p.ContactDetails)
                                        .Property(p => p.MobilePhone).HasColumnName("MobilePhone");
            builder.OwnsOne(p => p.ContactDetails)
                                        .Property(p => p.EmailId).HasColumnName("EmailId");
        }
    }
}
