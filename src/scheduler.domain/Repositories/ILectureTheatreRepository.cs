﻿using scheduler.domain.models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.domain.Repositories
{
    public interface ILectureTheatreRepository
    {
        Task<LectureTheatre> Add(LectureTheatre lectureTheatre);
        Task<List<LectureTheatre>> FindAll();
        Task<LectureTheatre> FindById(int id);
        Task Update(LectureTheatre student);
    }
}
