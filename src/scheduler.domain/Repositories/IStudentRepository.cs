﻿using scheduler.domain.models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.domain.Repositories
{
    public interface IStudentRepository
    {
        Task<Student> Add(Student student);
        Task<List<Student>> FindAll();
        Task<Student> FindById(int id);
        Task Update(Student student);
    }
}