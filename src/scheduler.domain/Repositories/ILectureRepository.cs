﻿using scheduler.domain.models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.domain.Repositories
{
    public interface ILectureRepository
    {
        Task<Lecture> Add(Lecture lecture);
        Task<List<Lecture>> FindAll();
        Task<Lecture> FindById(int id);
        Task Update(Lecture lecture);
    }
}
