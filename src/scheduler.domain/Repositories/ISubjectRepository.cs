﻿using scheduler.domain.models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace scheduler.domain.Repositories
{
    public interface ISubjectRepository
    {
        Task<Subject> Add(Subject subject);
        Task<List<Subject>> FindAll();
        Task<Subject> FindById(int id);
        void Update(Subject subject);
    }
}