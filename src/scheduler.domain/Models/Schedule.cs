using System;
using scheduler.common;

namespace scheduler.domain.models
{
    public class Schedule : ValueObject<Schedule>
    {
        public Schedule(DateTime start, Double duration)
        {
            Start = start;
            DurationInMins = duration;
        }
        public DateTime Start { get; private set; }
        public Double DurationInMins { get; private set; }
        public DateTime End => Start.AddMinutes(DurationInMins);
    }
}