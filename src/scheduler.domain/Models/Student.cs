using System.Collections.Generic;
using scheduler.common;

namespace scheduler.domain.models
{
    public class Student : IEntity
    {
        public Student()
        {

        }
        public Student(FullName fullName, ContactDetails contactDetails)
        {
            FullName = fullName;
            ContactDetails = contactDetails;
            _enrollments = new List<Lecture>();
        }
        public int Id { get; set; }
        public FullName FullName { get; private set; }
        public ContactDetails ContactDetails  { get; private set; }

        private List<Lecture> _enrollments;
        public IReadOnlyCollection<Lecture> Enrollments => _enrollments;

        public void EnrollForLecture(Lecture lecture)
        {
            _enrollments.Add(lecture);
        }
    }
}