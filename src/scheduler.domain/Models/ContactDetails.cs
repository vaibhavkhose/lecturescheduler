using scheduler.common;

namespace scheduler.domain.models
{
    public class ContactDetails : ValueObject<ContactDetails>
    {
        public ContactDetails(string mobilePhone, string emailId)
        {
            MobilePhone = mobilePhone;
            EmailId = emailId;
        }
        public string MobilePhone { get; private set; }
        public string EmailId { get; private set; }
    }
}