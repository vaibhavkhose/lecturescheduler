using scheduler.common;
using System.Collections.Generic;

namespace scheduler.domain.models
{
    public class Lecture : IEntity
    {
        public int Id { get; set; }
        public Schedule Period { get; set; }
        public Subject Subject { get; set; }
        public LectureTheatre Theatre { get; set; }
        public ICollection<Student> Enrollments { get; set; }
    }
}