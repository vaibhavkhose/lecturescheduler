using scheduler.common;
namespace scheduler.domain.models
{
    public class FullName : ValueObject<FullName>
    {
        public FullName(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public FullName(FullName fullName)
          : this(fullName.FirstName, fullName.LastName)
        {
        }

        internal FullName()
        {
        }


        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string AsFormattedName()
        {
            return this.FirstName + " " + this.LastName;
        }
    }
}