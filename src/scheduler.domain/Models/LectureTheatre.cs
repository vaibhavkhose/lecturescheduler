using System.Collections.Generic;
using scheduler.common;

namespace scheduler.domain.models
{
    public class LectureTheatre : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int Capacity { get; set; }
        public ICollection<Lecture> Lectures { get; set; }
    }
}