using System.Collections.Generic;
using scheduler.common;

namespace scheduler.domain.models
{
    public class Subject : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<Lecture> Lectures { get; set; }
    }
}