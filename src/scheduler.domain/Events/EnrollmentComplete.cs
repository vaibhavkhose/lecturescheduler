﻿using MediatR;

namespace scheduler.domain.Events
{
    public class EnrollmentComplete : INotification { }
}
