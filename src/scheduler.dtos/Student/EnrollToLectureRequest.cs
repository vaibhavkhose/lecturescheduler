﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace scheduler.dtos.Student
{
    public class EnrollToLectureRequest : IRequest<EnrollToLectureResponse>
    {
        public int StudentId { get; set; }
        public int LectureId { get; set; }
    }
}
