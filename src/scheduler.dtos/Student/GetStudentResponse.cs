using System;
using System.Collections.Generic;

namespace scheduler.dtos.Student
{
    public class GetStudentResponse
    {
        public List<StudentResponse> Students { get; set; }
    }

    public class StudentResponse
    {
        public string FullName { get; set; }
        public List<LectureResponse> Lectures { get; set; }

        public class LectureResponse
        {
            public string SubjectName { get; set; }
            public string LectureTheatreName { get; set; }
            public DateTime ScheduleDate { get; set; }
            public double DurationInMins { get; set; }
        }
    }
}