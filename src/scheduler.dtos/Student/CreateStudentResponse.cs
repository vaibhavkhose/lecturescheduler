namespace scheduler.dtos.Student
{
    public class CreateStudentResponse
    {
        public int StudentId { get; set; }
    }
}