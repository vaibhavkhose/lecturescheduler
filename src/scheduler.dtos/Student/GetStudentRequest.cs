using MediatR;

namespace scheduler.dtos.Student
{
    public class GetStudentRequest : IRequest<GetStudentResponse>
    {
    }
}