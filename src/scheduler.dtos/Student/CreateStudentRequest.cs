using MediatR;

namespace scheduler.dtos.Student
{
    public class CreateStudentRequest : IRequest<CreateStudentResponse>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string EmailId { get; set; }
    }
}