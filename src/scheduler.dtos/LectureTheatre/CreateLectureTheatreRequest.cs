using MediatR;

namespace scheduler.dtos.LectureTheatre
{
    public class CreateLectureTheatreRequest : IRequest<CreateLectureTheatreResponse>
    {
        public string Name { get; set; }
    }
}