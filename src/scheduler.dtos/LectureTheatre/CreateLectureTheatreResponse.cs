namespace scheduler.dtos.LectureTheatre
{
    public class CreateLectureTheatreResponse
    {
        public int LectureTheatreId { get; set; }
    }
}