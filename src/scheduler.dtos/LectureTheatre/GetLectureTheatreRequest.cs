using MediatR;

namespace scheduler.dtos.LectureTheatre
{
    public class GetLectureTheatreRequest : IRequest<GetLectureTheatreResponse>
    {
    }
}