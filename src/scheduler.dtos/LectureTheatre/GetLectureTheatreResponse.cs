using scheduler.dtos.Lecture;
using System;
using System.Collections.Generic;

namespace scheduler.dtos.LectureTheatre
{
    public class GetLectureTheatreResponse
    {
        public List<LectureTheatreResponse> LectureTheatres { get; set; }
    }

    public class LectureTheatreResponse
    {
        public string Name { get; set; }
        public List<LectureResponse> Lectures { get; set; }
    }
}