using MediatR;

namespace scheduler.dtos.Subject
{
    public class GetSubjectRequest : IRequest<GetSubjectResponse>
    {
    }
}