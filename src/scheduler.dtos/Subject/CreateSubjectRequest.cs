﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace scheduler.dtos.Subject
{
    public class CreateSubjectRequest : IRequest<CreateSubjectResponse>
    {
        public string Title { get; set; }
    }
}
