﻿using System;
using System.Collections.Generic;
using System.Text;

namespace scheduler.dtos.Subject
{
    public class CreateSubjectResponse
    {
        public int SubjectId { get; set; }
    }
}
