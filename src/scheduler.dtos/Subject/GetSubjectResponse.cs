using scheduler.dtos.Lecture;
using scheduler.dtos.LectureTheatre;
using System;
using System.Collections.Generic;

namespace scheduler.dtos.Subject
{
    public class GetSubjectResponse
    {
        public List<SubjectResponse> Subjects { get; set; }
    }

    public class SubjectResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<LectureResponse> Lectures { get; set; }
     
        public class LectureResponse
        {
            public DateTime Schedule { get; set; }
            public double DurationInMinutes { get; set; }
        }
    }
}