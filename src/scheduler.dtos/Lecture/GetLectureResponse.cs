using System;
using System.Collections.Generic;

namespace scheduler.dtos.Lecture
{
    public class GetLectureResponse
    {
        public List<LectureResponse> Lectures { get; set; }
    }

    public class LectureResponse
    {
        public string SubjectName { get; set; }
        public DateTime Schedule { get; set; }
        public double DurationInMinutes { get; set; }
        public string LectureTheatreName { get; set; }
        public List<EnrollmentResponse> Enrollments { get; set; }

        public class EnrollmentResponse
        {
            public string Name { get; set; }
            public string EmailId { get; set; }
        }

    }
}