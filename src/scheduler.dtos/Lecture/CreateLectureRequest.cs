﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace scheduler.dtos.Lecture
{
    public class CreateLectureRequest : IRequest<CreateLectureResponse>
    {
        public int SubjectId { get; set; }
        public int LectureTheatreId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public double DurationInMins { get; set; }
    }
}
