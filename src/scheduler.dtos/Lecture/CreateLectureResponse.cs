﻿using System;
using System.Collections.Generic;
using System.Text;

namespace scheduler.dtos.Lecture
{
    public class CreateLectureResponse
    {
        public int LectureId { get; set; }
    }
}
